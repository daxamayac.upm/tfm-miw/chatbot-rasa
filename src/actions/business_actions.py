import json
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from .services import product_services


class ActionGetPrice(Action):

    def name(self) -> Text:
        return "action_get_price"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        product = tracker.get_slot("product")
        product_db = product_services.read_by_name(name=product)
        price = product_db["price"]
        dispatcher.utter_message(
            response="utter_price_of_product",
            price=f"{price:.2f}",
            product=str(product_db["name"]).capitalize(),
            unit=str(product_db["unit"])
        )
        return []


class ActionAskProduct(Action):

    def name(self) -> Text:
        return "action_ask_product"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        value = tracker.get_slot("suggestion")
        suggestions = product_services.similarity_name(value)
        buttons = []
        for name in suggestions:
            buttons.append({"title": name,
                            "payload": "/request_product" + json.dumps({"product": name})
                            })

        dispatcher.utter_message(text="Tal ves te refieres a uno de estos productos, o escríbelo de otra forma")
        dispatcher.utter_message(buttons=buttons)
        return []
