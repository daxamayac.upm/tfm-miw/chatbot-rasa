import logging

from ..data.advisor_data import Advisor
from ..data.organization_data import Organization
from ..data.product_data import Product


def delete_all_and_seed_database():
    org = [
        Organization(
            name="vixlum",
            email="info@mongomock.com",
            address="Av. Universitaria Oe5-129 y Bolivia, Quito",
            services=[
                "Corte láser",
                "Impresión y escaneo en gran formato",
                "Diseño gráfico",
                "Personalización de productos en MDF",
                "Rompecabezas personalizados"
            ],
            businessHours={
                "weekday": "09h00 - 17h00",
                "saturday": "09h00 - 13h00"
            }
        )]

    advisors = [
        Advisor(name="David", slack_uuid="slackID", enabled=True)]

    products = [
        Product(name="llavero", price=10.00, uuid="uuid-llavero"),
        Product(name="agenda", price=5.00, uuid="uuid-agenda"),
        Product(name="billetera", price=25.00, uuid="uuid-billetera"),
        Product(name="corte laser", price=0.30, uuid="uuid-corte")
    ]

    logging.info("Delete all and seed database... ")
    Organization.drop_collection()
    Organization.objects.insert(org)

    Advisor.drop_collection()
    Advisor.objects.insert(advisors)

    Product.drop_collection()
    Product.objects.insert(products)
