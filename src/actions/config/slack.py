from slack_bolt import App

from ..config.config import config


class Slack:
    __instance = None

    def __init__(self):
        self.app = App(token=config.SLACK_BOT_TOKEN)

    @staticmethod
    def app():
        if Slack.__instance is None:
            Slack.__instance = Slack()
        return Slack.__instance.app
