import logging

from mongoengine import disconnect, connect

from .config import config
from .seeder_test import delete_all_and_seed_database


def start_database():
    logging.getLogger().setLevel(logging.INFO)
    logging.info("load config")
    disconnect()
    connect('vixlum-dev', host=config.DATA_HOST)
    if config.ENVIRONMENT in ["test"]:  # "prod" only because it is staging
        delete_all_and_seed_database()
