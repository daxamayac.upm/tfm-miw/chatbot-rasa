from mongoengine import StringField, ReferenceField, ListField, Document

from .advisor_data import Advisor


class Organization(Document):
    name = StringField(required=True)
    email = StringField()
    address = StringField(required=True)
    services = StringField(required=True)
    businessHours = StringField(required=True)
    advisors = ListField(ReferenceField(Advisor))

    def __repr__(self):
        return str(self.dict())

    def dict(self):
        return {'id': str(self.id), 'name': self.name, 'address': self.address, 'services': self.services,
                'businessHours': self.businessHours}


def find_by_name(name):
    organization_document = Organization.objects(name=name).get()
    return organization_document.dict()
