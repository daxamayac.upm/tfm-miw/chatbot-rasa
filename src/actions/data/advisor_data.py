from mongoengine import StringField, BooleanField, Document


class Advisor(Document):
    name = StringField(required=True)
    slack_uuid = StringField(required=True)
    enabled = BooleanField(required=True)

    def __repr__(self):
        return str(self.dict())

    def dict(self):
        return {'id': str(self.id), 'name': self.name, 'slack_uuid': self.slack_uuid, 'enabled': self.enabled}


def find_by_enabled(enabled):
    advisors = []
    for item in Advisor.objects(enabled=enabled):
        advisors.append(item.dict())
    return advisors
