from mongoengine import StringField, Document, DecimalField


class Product(Document):
    name = StringField(required=True)
    price = DecimalField(required=True)
    uuid = StringField(required=True)
    unit = StringField(default='unidad')
    details = StringField()
    similar = StringField()

    def __repr__(self):
        return str(self.dict())

    def dict(self):
        return {'id': str(self.id), 'name': self.name, 'price': self.price, 'uuid': self.uuid, 'unit': self.unit}


def read_by_name(name):
    products = []
    for item in Product.objects(name=name):
        products.append(item.dict())
    return products


def find_all() -> [Product]:
    products = Product.objects()
    return products
