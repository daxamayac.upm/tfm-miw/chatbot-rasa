import uuid
from datetime import date
from typing import Text, Dict, Any, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher

from .services import human_handoff_services


class ActionHandoffOptions(Action):
    def name(self) -> Text:
        return "action_handoff_options"

    def run(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[EventType]:
        buttons = [
            {"title": "Yes", "payload": "Yes"},
            {"title": "No", "payload": "No"}
        ]
        dispatcher.utter_message(buttons=buttons)

        return []


class ActionHandoff(Action):
    def name(self) -> Text:
        return "action_handoff"

    def run(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[EventType]:

        advisor = human_handoff_services.get_advisor()

        if advisor is not None:
            name = date.today().strftime("%Y-%m-%d--" + str(uuid.uuid1()))
            wss = human_handoff_services.get_wss()
            token = human_handoff_services.get_token()
            channel = human_handoff_services.create_conversation(name=name, topic='', advisor=advisor)

            dispatcher.utter_message(
                response="utter_yes_handoff",
                json_message={
                    "channel": channel,
                    "title": "Soy tu asesor",
                    "wss": wss,
                    "token": token
                }
            )
        else:
            dispatcher.utter_message(response="utter_no_handoff")

        return []


class ActionHandoffWss(Action):
    def name(self) -> Text:
        return "action_handoff_ws"

    def run(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[EventType]:
        wss = human_handoff_services.get_wss()
        dispatcher.utter_message(
            json_message={
                "wss": wss
            }
        )
        return []
