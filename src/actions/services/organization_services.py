import logging

from ..data import organization_data

NOT_FOUND = "Información no encontrada"


def read_address():
    try:
        response = organization_data.find_by_name("vixlum")
        resp = response['address']
    except TypeError as err:
        logging.error(err)
        resp = NOT_FOUND

    return resp


def read_business_hours():
    try:
        response = organization_data.find_by_name("vixlum")['businessHours']
        resp = "Abierto "
        if "weekday" in response:
            resp += "lunes a viernes de " + response['weekday'] + ", "
        if "saturday" in response:
            resp += "sábado de " + response['saturday']

    except TypeError as err:
        logging.error(err)
        resp = NOT_FOUND

    return resp


def read_services():
    try:
        response = organization_data.find_by_name("vixlum")
        resp = ""
        for i in response['services']:
            resp += i + "\n"

    except TypeError as err:
        logging.error(err)
        resp = NOT_FOUND

    return resp
