import logging
import difflib

from ..data import product_data
from ..data.product_data import Product

NOT_FOUND = "Información no encontrada"


def find_all_name():
    resp = []
    try:
        response = product_data.find_all()
        for item in response:
            resp.append(item["name"])

    except TypeError as err:
        logging.error(err)
        resp = NOT_FOUND

    return resp


def read_by_name(name) -> Product:
    try:
        resp = product_data.read_by_name(name=name)
    except TypeError as err:
        logging.error(err)
        resp = NOT_FOUND

    return resp[0]


def similarity_name(name) -> list:
    resp = {}
    products = product_data.find_all()
    for product in products:
        ratio = difflib.SequenceMatcher(a=product["name"].lower(), b=name).quick_ratio()
        resp[product["name"]] = ratio

    resp = dict(sorted(resp.items(), key=lambda item: item[1], reverse=True))

    return list(resp)[:3]
