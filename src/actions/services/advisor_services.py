from ..data import advisor_data


def find_by_enabled(enabled):
    return advisor_data.find_by_enabled(enabled=enabled)
