import logging
import random

from . import slack_services, advisor_services


def create_conversation(name, topic, advisor):
    try:
        channel = slack_services.conversations_create(name=name)
        slack_services.set_topic(channel=channel, topic=topic)
        slack_services.conversations_invite(channel=channel, users=advisor)
        post_message(channel=advisor,
                     message_text="Estimado asesor se le ha asignado un caso, ppor favor revisa tus canales :)")
        post_message(channel=advisor,
                     message_text="En breve será transferida la conversación realizada con el cliente para que puedas continuar con la atención")
    except Exception as err:
        logging.error('##error')
        logging.error(err)
    return channel


def post_message(channel, message_text):
    slack_services.post_message(channel=channel, message_text=message_text)


def get_advisor():
    resp = None
    advisors = advisor_services.find_by_enabled(enabled=True)
    advisors_available = []
    for advisor in advisors:
        slack_advisor = slack_services.check_status_advisor(advisor['slack_uuid'])
        if slack_advisor['presence'] == 'active':
            advisors_available.append(advisor)

    if len(advisors_available) > 0:
        advisor = advisors_available[__random_advisor(len(advisors_available))]
        resp = advisor['slack_uuid']

    return resp


def get_wss():
    return slack_services.request_wss()


def get_token():
    return slack_services.get_token()


def __random_advisor(size):
    return random.randint(0, size - 1)
