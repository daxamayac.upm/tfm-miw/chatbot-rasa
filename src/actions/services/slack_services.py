import json

import requests

from slack_sdk.errors import SlackApiError

from ..config.config import config
from ..config.slack import Slack

SLACK_URL = "https://slack.com/api/apps.connections.open"


def check_status_advisor(slack_uuid):
    try:
        response = Slack.app().client.users_getPresence(user=slack_uuid)
    except SlackApiError as err:
        raise Exception('Slack Error:' + err.response['error'])
    return response


def conversations_create(name):
    response = Slack.app().client.conversations_create(name=name)
    return response.get('channel')['id']


def set_topic(channel, topic):
    response = Slack.app().client.conversations_setTopic(channel=channel, topic=topic)
    return response


def conversations_invite(channel, users):
    response = Slack.app().client.conversations_invite(channel=channel, users=users)
    return response


def post_message(channel, message_text):
    response = Slack.app().client.chat_postMessage(channel=channel, username='Vixi', text=message_text)
    return response


def request_wss():
    payload = {}
    headers = {
        'Authorization': "Bearer " + config.SLACK_APP_TOKEN
    }

    response = requests.request("POST", SLACK_URL, headers=headers, data=payload)
    resp = json.loads(response.text)
    return resp['url']


def get_token():
    return config.SLACK_BOT_TOKEN[5:]
