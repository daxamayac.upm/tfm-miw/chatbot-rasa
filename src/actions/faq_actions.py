from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from .config.database import start_database
from .services.organization_services import read_address, read_services, read_business_hours

start_database()


class ActionLocation(Action):

    def name(self) -> Text:
        return "action_location"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text=read_address())
        return []


class ActionServices(Action):

    def name(self) -> Text:
        return "action_services"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text=read_services())
        return []


class ActionBusinessHours(Action):

    def name(self) -> Text:
        return "action_business_hours"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text=read_business_hours())

        return []
