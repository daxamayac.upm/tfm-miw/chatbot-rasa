from typing import Dict, Text, Any

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction

from .services import product_services


class ValidateProductForm(FormValidationAction):

    def name(self) -> Text:
        return "validate_product_form"

    def validate_product(
            self,
            value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:

        products = product_services.find_all_name()
        if value.lower() in products:
            return {"product": value}
        else:
            dispatcher.utter_message(response="utter_wrong_product")
            return {"product": None, "suggestion": value}
