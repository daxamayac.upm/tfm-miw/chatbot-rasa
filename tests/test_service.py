import unittest
from unittest import mock

from mongoengine import disconnect

from src.actions.services import advisor_services, human_handoff_services, product_services, organization_services
from src.actions.config.database import start_database


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        start_database()

    def test_read_address(self):
        self.assertEqual('Av. Universitaria Oe5-129 y Bolivia, Quito', organization_services.read_address())

    def test_read_services(self):
        self.assertIn('Corte l', organization_services.read_services())

    def test_read_hours(self):
        self.assertIn('09h00 - 17h00', organization_services.read_business_hours())
        self.assertIn('09h00 - 13h00', organization_services.read_business_hours())

    def test_find_enabled(self):
        resp = advisor_services.find_by_enabled(enabled=True)
        self.assertIn('slackID', str(resp))

    @mock.patch('src.actions.services.slack_services.check_status_advisor')
    def test_get_advisor_no_results(self, check_status_advisor):
        resp = human_handoff_services.get_advisor()
        self.assertEqual(None, resp)
        check_status_advisor.assert_called()

    @mock.patch('src.actions.services.slack_services.check_status_advisor')
    def test_get_advisor_advisors_presence_active(self, check_status_advisor):
        check_status_advisor.return_value = dict({'ok': True, 'presence': 'active'})
        resp = human_handoff_services.get_advisor()
        self.assertEqual('slackID', resp)
        check_status_advisor.assert_called()

    @mock.patch('src.actions.services.slack_services.post_message')
    def test_post_message(self, post_message):
        human_handoff_services.post_message(channel='', message_text='')
        post_message.assert_called()

    @mock.patch('src.actions.services.slack_services.conversations_create')
    @mock.patch('src.actions.services.slack_services.set_topic')
    @mock.patch('src.actions.services.slack_services.conversations_invite')
    @mock.patch('src.actions.services.slack_services.post_message')
    def test_create_conversation(self, conversations_create, set_topic, conversations_invite,
                                 post_message):
        human_handoff_services.create_conversation(name='', topic='', advisor='')
        conversations_create.assert_called()
        set_topic.assert_called()
        conversations_invite.assert_called()
        post_message.assert_called()

    def test_find_all_products(self):
        products = product_services.find_all_name()
        self.assertIn('llavero', products)

    def test_read_by_name_product(self):
        product = product_services.read_by_name(name='llavero')
        self.assertEqual('llavero', product["name"])
        self.assertEqual(10.00, product["price"])

    def test_similarity_name_product(self):
        name_products = product_services.similarity_name(name='cort')
        self.assertIn('corte laser', name_products)

