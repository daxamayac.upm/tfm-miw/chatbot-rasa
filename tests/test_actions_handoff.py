import unittest
from unittest import mock

from mongoengine import disconnect
from rasa_sdk.executor import CollectingDispatcher

from src.actions.config.database import start_database
from src.actions.handoff_actions import ActionHandoff, ActionHandoffWss, ActionHandoffOptions


class TestActionLocation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        start_database()

    def testActionHandoffNameFunction(self):
        name = ActionHandoff().name()
        self.assertEqual("action_handoff", name)

    @mock.patch('src.actions.services.slack_services.check_status_advisor',
                return_value=dict({'ok': True, 'presence': 'active'}))
    @mock.patch('src.actions.services.human_handoff_services.create_conversation',
                return_value="channelIDCreated")
    @mock.patch('src.actions.services.human_handoff_services.get_wss',
                return_value="wss")
    @mock.patch('src.actions.services.human_handoff_services.get_token',
                return_value="token")
    def testActionHandoffYes(self, check_status_advisor, create_conversation, get_wss, get_token):
        dispatcher = CollectingDispatcher()
        ActionHandoff().run(dispatcher, None, None)
        self.assertEqual("utter_yes_handoff", dispatcher.messages[0]["template"])
        self.assertEqual("channelIDCreated", dispatcher.messages[0]["custom"]["channel"])
        check_status_advisor.assert_called()
        create_conversation.assert_called()
        get_wss.assert_called()
        get_token.assert_called()

    @mock.patch('src.actions.services.human_handoff_services.get_advisor',
                return_value=None)
    def testActionHandoffNo(self, get_advisor):
        dispatcher = CollectingDispatcher()
        ActionHandoff().run(dispatcher, None, None)
        self.assertEqual("utter_no_handoff", dispatcher.messages[0]["template"])
        get_advisor.assert_called()

    def testActionHandoffWssNameFunction(self):
        name = ActionHandoffWss().name()
        self.assertEqual("action_handoff_ws", name)

    @mock.patch('src.actions.services.human_handoff_services.get_wss',
                return_value="wss")
    def testActionHandoffWss(self, get_wss):
        dispatcher = CollectingDispatcher()
        ActionHandoffWss().run(dispatcher, None, None)
        self.assertEqual("wss", dispatcher.messages[0]["custom"]["wss"])
        get_wss.assert_called()

    def testActionHandoffOptionsNameFunction(self):
        name = ActionHandoffOptions().name()
        self.assertEqual("action_handoff_options", name)

    def testActionHandoffOptions(self):
        dispatcher = CollectingDispatcher()
        ActionHandoffOptions().run(dispatcher, None, None)
        buttons = [
            {"title": "Yes", "payload": "Yes"},
            {"title": "No", "payload": "No"}
        ]
        self.assertEqual(buttons, dispatcher.messages[0]["buttons"])
