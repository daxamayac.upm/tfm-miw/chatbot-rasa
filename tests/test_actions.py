import unittest
from typing import Text

from mongoengine import disconnect
from rasa_sdk.executor import CollectingDispatcher

from src.actions.faq_actions import ActionLocation, ActionServices, ActionBusinessHours
from src.actions.business_actions import ActionGetPrice, ActionAskProduct
from src.actions.config.database import start_database
from src.actions.validate_product_form import ValidateProductForm


class TestActionLocation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        start_database()

    def testActionLocation(self):
        dispatcher = CollectingDispatcher()
        ActionLocation().run(dispatcher, None, None)
        self.assertIn("Universitaria", str(dispatcher.messages))

    def testActionLocationFunctionName(self):
        name = ActionLocation().name()
        self.assertEqual("action_location", name)

    def testActionServices(self):
        dispatcher = CollectingDispatcher()
        ActionServices().run(dispatcher, None, None)
        self.assertIn("Corte", str(dispatcher.messages))

    def testActionServicesFunctionName(self):
        name = ActionServices().name()
        self.assertEqual("action_services", name)

    def testActionBusinessHours(self):
        dispatcher = CollectingDispatcher()
        ActionBusinessHours().run(dispatcher, None, None)
        self.assertIn("13h00", str(dispatcher.messages))

    def testActionBusinessHoursFunctionName(self):
        dispatcher = CollectingDispatcher()
        name = ActionBusinessHours().name()
        self.assertEqual("action_business_hours", name)

    def testValidateNoFound(self):
        dispatcher = CollectingDispatcher()
        text = Text("noEstoyEnCatalogo")
        ValidateProductForm().validate_product(text, dispatcher, None, None)
        self.assertEqual("utter_wrong_product", dispatcher.messages[0]["template"])

    def testValidateFunctionName(self):
        name = ValidateProductForm().name()
        self.assertEqual("validate_product_form", name)

    def testValidateFound(self):
        dispatcher = CollectingDispatcher()
        text = Text("llavero")
        value = ValidateProductForm().validate_product(text, dispatcher, None, None)
        self.assertEqual({"product": "llavero"}, value)

    def testGetPriceFunctionName(self):
        name = ActionGetPrice().name()
        self.assertEqual("action_get_price", name)
        
    def testActionAskProductFunctionName(self):
        name = ActionAskProduct().name()
        self.assertEqual("action_ask_product", name)
