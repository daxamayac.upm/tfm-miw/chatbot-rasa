## Tecnologías necesarias
`Python 3.8.10` `Rasa 2.6.2` `GitLab` `GitLab CI/CD` `SonarCloud` `Slack` `Heroku` `MongoDB` `Docker`


## Estado del código

 GitLab CI/CD | SonarCloud  
 -- | --
 [![pipeline status](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-rasa/badges/develop/pipeline.svg)](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-rasa/-/commits/develop) | [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daxamayac.upm_chatbot-rasa&metric=alert_status)](https://sonarcloud.io/dashboard?id=daxamayac.upm_chatbot-rasa)

Rasa Test

```sh
> rasa test --stories ../tests
> rasa data split nlu
> rasa test nlu --nlu train_test_split/test_data.yml

```

![Bot](src/results/intent_confusion_matrix.png)
![Bot](src/results/intent_histogram.png)


### Más documentación en

[Docs](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-docs)
